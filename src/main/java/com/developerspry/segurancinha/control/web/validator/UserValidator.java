/**
 * 
 */
package com.developerspry.segurancinha.control.web.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.developerspry.segurancinha.common.validator.SegurancinhaValidator;
import com.developerspry.segurancinha.control.model.User;
import com.developerspry.segurancinha.control.service.interfaces.IUserService;

/**
 * @author rubens.ferreira
 *
 */
@Component
public class UserValidator extends SegurancinhaValidator<User> {

	@Autowired
	private IUserService userService;

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.isAssignableFrom(clazz);
	}

	@Override
	public void validateEntity(Object object, Errors errors) {
		User user = (User) object;

		if (!objectIsNull(user, errors)) {

			// METODO PARA UPDATE
			if (user.getId() > 0) {
				if (!objectNotExist(userService.findById(user.getId()), errors)) {

				}
			} else {
				isNullOrEmpty("password", user.getPassword(), errors);
			}
			
			if (!isNullOrEmpty("login", user.getLogin(), errors)) {
				checkSize("login", user.getLogin(), 1, 50, errors);
			}
			
			if (!isNullOrEmpty("name", user.getName(), errors)) {
				checkSize("name", user.getName(), 1, 1000, errors);
			}
			
			if (!isNullOrEmpty("email", user.getEmail(), errors)) {
				checkSize("name", user.getEmail(), 1, 1000, errors);
			}
			
			if (!isNullOrEmpty("phone", user.getPhone(), errors)) {
				checkSize("phone", user.getPhone(), 1, 14, errors);
			}
			
			if (!isNullOrEmpty("profileType", user.getProfileType(), errors)) {
				checkSize("profileType", user.getProfileType(), 1, 14, errors);
			}
			
		}
	}

}
