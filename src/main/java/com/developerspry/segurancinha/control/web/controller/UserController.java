/**
 * 
 */
package com.developerspry.segurancinha.control.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.developerspry.segurancinha.common.handler.exceptions.GenericValidationException;
import com.developerspry.segurancinha.common.security.util.LoggedUserUtil;
import com.developerspry.segurancinha.control.model.User;
import com.developerspry.segurancinha.control.service.interfaces.IUserService;
import com.developerspry.segurancinha.control.web.validator.UserValidator;

/**
 * @author rubens.ferreira
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService service;

	@Autowired
	private UserValidator validator;

	@InitBinder
	protected void initiBinder(WebDataBinder binder, HttpServletRequest request) {
		validator.setMethod(RequestMethod.valueOf(request.getMethod()));
		binder.addValidators(validator);
	}

	@PostMapping(value = { "/", "" })
	public ResponseEntity<User> save(@RequestBody @Valid User user, BindingResult result,
			HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Save User");
		}
		service.save(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@PutMapping(value = { "/", "" })
	public User update(@RequestBody @Valid User user, BindingResult result, HttpServletResponse response) {
		if (result.hasErrors()) {
			throw new GenericValidationException(result, "Error to Update User");
		}
		service.save(user);
		return user;
	}

	@DeleteMapping(value = { "/{id}" })
	public boolean delete(@PathVariable("id") Integer id) {
		try {
			User user = service.findById(id);
			return service.delete(user);
		} catch (Exception e) {
			throw new GenericValidationException(null, "Error to Delete User");
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<User> findById(@PathVariable("id") Integer id) {
		return new ResponseEntity<User>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping(value = { "/", "" })
	public ResponseEntity<List<User>> findAllByOrderByNameAsc() {
		return new ResponseEntity<List<User>>(service.findAllByOrderByNameAsc()
				.stream().filter(user -> LoggedUserUtil.getUser().getProfileType().equalsIgnoreCase("GERENTE") || user.getId() == LoggedUserUtil.getUser().getId()).collect(Collectors.toList()), HttpStatus.OK);
	}

}
