/**
 * 
 */
package com.developerspry.segurancinha.control.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens.ferreira
 *
 */
public interface IUserDAO extends CrudRepository<User, Serializable> {

	User findByLoginAndPassword(String login, String password);
	
	User findByLogin(String login);
	
	List<User> findAllByOrderByNameAsc();
	
}
