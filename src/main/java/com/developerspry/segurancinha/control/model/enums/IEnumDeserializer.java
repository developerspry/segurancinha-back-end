package com.developerspry.segurancinha.control.model.enums;

public interface IEnumDeserializer<T> {
	
	T getTextEnumByString(String text);
	
}
