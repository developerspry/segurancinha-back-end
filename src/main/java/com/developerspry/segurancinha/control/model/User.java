package com.developerspry.segurancinha.control.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.developerspry.segurancinha.common.config.data.serializers.DecryptSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author Renatão
 *
 */
@Entity
@SequenceGenerator(initialValue = 1, name = "base_gen", sequenceName = "user_seq")
@Table(name = "users")
@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id_user")) })
public class User extends GenericEntity implements Authenticated {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "ds_login", nullable = false, length = 50)
	private String login;

	@Column(name = "ds_password", nullable = false)
	private String password;

	@Column(name = "nm_user", nullable = false, length = 1000)
	private String name;

	@JsonSerialize(using = DecryptSerializer.class)
	@Column(name = "ds_email", nullable = false, length = 1000)
	private String email;

	@JsonSerialize(using = DecryptSerializer.class)
	@Column(name = "nr_telefone", nullable = false)
	private String phone;

	@Column(name = "ds_profile_type", nullable = false, length = 7)
	private String profileType;

	public User() {
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@JsonProperty(access = Access.WRITE_ONLY)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

}