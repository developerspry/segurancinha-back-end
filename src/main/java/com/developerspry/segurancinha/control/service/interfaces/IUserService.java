package com.developerspry.segurancinha.control.service.interfaces;

import java.util.List;

import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens_ferreira
 *
 */
public interface IUserService extends IService<User> {

	User findByLoginAndPassword(String login, String password);
	
	User findByLogin(String login);
	
	List<User> findAllByOrderByNameAsc();
	
}
