package com.developerspry.segurancinha.control.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.developerspry.segurancinha.control.dao.IUserDAO;
import com.developerspry.segurancinha.control.model.User;
import com.developerspry.segurancinha.control.service.interfaces.IUserService;

/**
 * @author rubens_ferreira
 *
 */
@Service
@Transactional
public class UserServiceImpl extends ServiceImpl<User, IUserDAO> implements IUserService {

	@Autowired
	private IUserDAO dao;
	
	@Override
	public User findByLoginAndPassword(String login, String password) {
		return dao.findByLoginAndPassword(login, password);
	}

	@Override
	public User findByLogin(String login) {
		return dao.findByLogin(login);
	}

	@Override
	public List<User> findAllByOrderByNameAsc(){
		return dao.findAllByOrderByNameAsc();
	}
	
}
