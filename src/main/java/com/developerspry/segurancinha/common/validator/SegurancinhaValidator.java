package com.developerspry.segurancinha.common.validator;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestMethod;

import com.developerspry.segurancinha.common.validator.util.StringValidator;
import com.developerspry.segurancinha.control.model.GenericEntity;

public abstract class SegurancinhaValidator<T extends GenericEntity> implements Validator {

	private Class<T> clazz;
	private RequestMethod method;

	@SuppressWarnings("unchecked")
	public SegurancinhaValidator() {
		clazz = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	public boolean isInsert() {
		return RequestMethod.POST == getMethod();
	}

	public boolean isUpdate() {
		return RequestMethod.PUT == getMethod();
	}

	@Override
	public void validate(Object object, Errors errors) {
		GenericEntity entity = (GenericEntity) object;
		if (entity.getId() <= 0 && !isInsert())
			errors.reject(clazz.getSimpleName().toLowerCase() + "GlobalError.updateError",
					clazz.getSimpleName() + " Register does not exist!");
		else if (entity.getId() > 0 && !isUpdate()) {
			errors.reject(clazz.getSimpleName().toLowerCase() + "GlobalError.insertError", clazz.getSimpleName()
					+ " Register already exists! (change the id to '0', so that a new record is generated)");
		} else
			validateEntity(object, errors);
	}

	public abstract void validateEntity(Object object, Errors errors);

	public RequestMethod getMethod() {
		return method;
	}

	public void setMethod(RequestMethod method) {
		this.method = method;
	}

	public boolean objectNotExist(T value, Errors errors) {
		if (Objects.isNull(value)) {
			errors.reject(clazz.getSimpleName().toLowerCase() + "GlobalError.objectNotExist",
					"The object does not exist in the database.");
			return true;
		}
		return false;
	}

	public boolean isEqualZero(String fieldName, int value, Errors errors) {
		if (value == 0) {
			errors.rejectValue(clazz.getSimpleName().toLowerCase() + "." + fieldName + ".emptyObject",
					"The value result in zero.");
			return true;
		}
		return false;
	}

	// VERIFICAR SE O OBJETO E NULO
	@SuppressWarnings("unchecked")
	public boolean objectIsNull(String fieldName, Object value, Errors errors) {
		if ((Objects.isNull(value)) || ((value instanceof ArrayList) && ((ArrayList<Object>) value).size() == 0)) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".emptyObject",
					"You must complete the information");
			return true;
		}
		return false;
	}

	// VERIFICAR SE O OBJETO E NULO
	public <Type> boolean objectIsNull(Type value, Errors errors) {
		if (Objects.isNull(value)) {
			errors.reject(value.getClass().getSimpleName().toLowerCase() + ".GlobalError.emptyObject",
					"You must complete the information");
			return true;
		}
		return false;
	}

	// VERIFICAR SE O OBJETO E NULO
	public <Type> boolean objectIsNull(Type value) {
		if (Objects.isNull(value)) {
			return true;
		}
		return false;
	}

	// VERIFICAR SE E NULO
	public <Type> boolean isNotNullOrEmpty(Collection<Type> value) {
		if (Objects.nonNull(value) && !value.isEmpty()) {
			return true;
		}
		return false;
	}

	// VERIFICAR CAMPO SE ESTA NULL OU VAZIO COM ERROS
	public boolean isNullOrEmpty(String fieldName, String value, Errors errors) {
		if (Objects.isNull(value) || value.trim().isEmpty()) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".empty",
					"The " + fieldName + " must be informed");
			return true;
		}
		return false;
	}

	// VERIFICAR CAMPO SE ESTA NULL OU VAZIO
	public boolean isNullOrEmpty(String value) {
		return (Objects.isNull(value) || value.trim().isEmpty()) ? true : false;
	}

	// VERIFICAR SE HA CARACTERES ESPECIAIS E ESPACO
	public boolean containsSpaceAndSpecialCharacter(String fieldName, String value, Errors errors) {
		String specialAndSpaces = "!@#$%^&*() _";
		String pattern = ".*[" + Pattern.quote(specialAndSpaces) + "].*";
		if (value == null || value.matches(pattern)) {
			errors.rejectValue(fieldName,
					clazz.getSimpleName().toLowerCase() + "." + fieldName + ".spaceEspecialCharacter",
					"The " + fieldName + " is required and must not contain special characters and no space.");
			return true;
		}
		return false;
	}

	// VERIFICAR SE HA CARACTERES ESPECIAIS
	public boolean containsSpecialCharacter(String fieldName, String value, Errors errors) {
		String specialAndSpaces = "!@#$%^&*()";
		String pattern = ".*[" + Pattern.quote(specialAndSpaces) + "].*";
		if (value == null || value.matches(pattern)) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".specialCharacter",
					"The " + fieldName + " is required and must not contain special characters.");
			return true;
		}
		return false;
	}

	// VERIFICAR SE TAMANHO E VALIDO
	public boolean checkSize(String fieldName, String value, long min, long max, Errors errors) {
		if (value == null || value.length() < min || value.length() > max) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".invalidSize",
					"The " + fieldName + " must be larger than or equal to " + min + " and less equal to " + max);
			return true;
		}
		return false;
	}

	// VERIFICAR SE CATEGORIA É VÁLIDA
	public boolean checkProfileCategoryEnum(String fieldName, String value, Errors errors) {
		
		boolean find = false;
		
		if(!find)
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".invalidProfileCategory",
					"Category " + fieldName + " does not exist");
			
		return false;
	}

	// VERIFICAR SE VALOR ESTÁ NUM INTERVALO VÁLIDO
	public boolean checkInterval(String fieldName, int value, long min, long max, Errors errors) {
		if (value == 0 || value < min || value > max) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".invalidInterval",
					"The " + fieldName + " must be larger than or equal to " + min + " and less equal to " + max);
			return true;
		}
		return false;
	}

	public boolean noHasOnlyAlphaNumericAndUpperCaseAndNotSpace(String fieldName, String value, Errors errors) {
		if (!StringValidator.onlyAlphaNumericAndUpperCaseAndNotSpace(value)) {
			errors.rejectValue(fieldName,
					clazz.getSimpleName().toLowerCase() + "." + fieldName + ".alphaNumericNoSpacesAndUpperCase",
					"The " + fieldName + " must be alphanumeric, uppercase characters and may not have space");
			return true;
		}
		return false;
	}

	// VERIFICAR SE DATA ESTA NULA
	public boolean isDateNull(String fieldName, Date value, Errors errors) {
		if (value == null) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".dateEmpty",
					"The " + fieldName + " is required.");
			return true;
		}
		return false;
	}

	// VERIFICAR SE HA DIFERENCA NO TAMANHO
	public boolean checkDiferentSize(String fieldName, String value, long tamanho, Errors errors) {
		if (fieldName == null || value.trim().length() != tamanho) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".invalidSize",
					"The " + fieldName + " should have a length of " + tamanho);
			return true;
		}
		return false;
	}

	// VERIFICAR SE CONTEM LETRAS MINUSCULAS
	public boolean containsLowerCase(String fieldName, String value, Errors errors) {
		if (value == null || !value.equals(value.toUpperCase())) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".lowerCase",
					"The " + fieldName + " must not contain lowercase characters.");
			return true;
		}
		return false;
	}

	// VERIFICAR SE CONTEM LETRAS MAIUSCULAS
	public boolean containsUpperCase(String fieldName, String value, Errors errors) {
		if (value == null || !value.equals(value.toLowerCase())) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".upperCase",
					"The " + fieldName + " is required and must not contain uppercase characters.");
			return true;
		}
		return false;
	}

	// VERIFICAR SE HA CAMPO REPETIDO
	public void repeatedField(String fieldName, String value, Errors errors) {
		errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".existing",
				"There is already record with that " + fieldName + " registered.");
	}

	// VERIFICAR SE CONTEM ESPACO
	public boolean containsSpace(String fieldName, String value, Errors errors) {
		if (StringValidator.containsSpace(value)) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".spaco",
					"The " + fieldName + " should not contain space");
			return true;
		}
		return false;
	}

	// VERIFIAR SE HA SOMENTE DIGITOS
	public boolean noHasOnlyDigits(String fieldName, String value, Errors errors) {
		if (!StringValidator.onlyDigits(value)) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".onlyDigits",
					"The " + fieldName + " must contain only digits");
			return true;
		}
		return false;
	}

	// VALIDAR SE DECIMAL E VALIDO
	public boolean isDecimalValid(String fieldName, String value, Errors errors) {
		if (!StringValidator.isFloatNumber(value)) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".realNumber",
					"The " + fieldName + " must contain only digits");
			return false;
		}
		return true;

	}

	// VERIFICAR E-MAIL
	public boolean isEmailValid(String fieldName, String value, Errors errors) {
		if (!StringValidator.isEmailValid(value)) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + ".emailInvalid",
					"The email must have the following standard email and have only tiny characters");
			return true;
		}
		return false;
	}

	// VERIFICAR CNPJ
	public boolean isCNPJValid(String cnpj, Errors errors) {

		boolean invalid = (cnpj.equals("00000000000000") || cnpj.equals("11111111111111")
				|| cnpj.equals("22222222222222") || cnpj.equals("33333333333333") || cnpj.equals("44444444444444")
				|| cnpj.equals("55555555555555") || cnpj.equals("66666666666666") || cnpj.equals("77777777777777")
				|| cnpj.equals("88888888888888") || cnpj.equals("99999999999999") || (cnpj.length() != 14));

		int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

		Integer d1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
		Integer d2 = calcularDigito(cnpj.substring(0, 12) + d1, pesoCNPJ);

		if (!cnpj.equals(cnpj.substring(0, 12) + d1.toString() + d2.toString()) || invalid) {
			errors.rejectValue("cnpj", clazz.getSimpleName().toLowerCase() + "." + "cnpj.invalid",
					"Informed cnpj not available");
			return true;
		}
		return false;
	}

	// VERIFICAR CPF
	public boolean isCPFValid(String cpf, Errors errors) {

		boolean invalid = (cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222")
				|| cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555")
				|| cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888")
				|| cpf.equals("99999999999") || (cpf.length() != 11));

		int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

		Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		if (!cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString()) || invalid) {
			errors.rejectValue("cpf", clazz.getSimpleName().toLowerCase() + "." + "cpf.invalid", "The CPF is invalid");
			return true;
		}
		return false;
	}

	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	protected boolean isNumberInvalid(String fieldName, int number, Errors errors) {
		if (number <= 0) {
			errors.rejectValue(fieldName, clazz.getSimpleName().toLowerCase() + "." + fieldName + ".invalid", "The " + fieldName + " must be greather than 0");
			return true;
		}
		return false;
	}

}