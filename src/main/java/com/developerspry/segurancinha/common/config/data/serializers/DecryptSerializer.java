package com.developerspry.segurancinha.common.config.data.serializers;

import java.io.IOException;
import java.util.Objects;

import com.developerspry.segurancinha.common.security.exception.InvalidKeyException;
import com.developerspry.segurancinha.common.security.manager.CryptoManager;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class DecryptSerializer extends StdSerializer<String> {

	private static final long serialVersionUID = 1L;

	public DecryptSerializer() {
		this(null);
	}

	protected DecryptSerializer(Class<String> t) {
		super(t);
	}

	@Override
	public void serialize(String text, JsonGenerator generator, SerializerProvider arg2) throws IOException {
		if (Objects.nonNull(text)) {
			String decryptedText = null;
			try {
				decryptedText = CryptoManager.decrypt(text);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(decryptedText)) {
				text = decryptedText;
			}
		}
		generator.writeString(text);
	}
}
