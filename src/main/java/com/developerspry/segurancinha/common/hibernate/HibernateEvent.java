package com.developerspry.segurancinha.common.hibernate;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.developerspry.segurancinha.common.hibernate.eventlistener.UserPreInsertLoadUpdateEventListener;
import com.developerspry.segurancinha.common.hibernate.eventlistener.BaseEntityPreInsertUpdateEventListener;
import com.developerspry.segurancinha.common.hibernate.eventlistener.PreInsertLoadUpdateEventListener;
import com.developerspry.segurancinha.control.model.GenericEntity;
import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens.ferreira Adiciona os listeners
 */
@Configuration
public class HibernateEvent {

	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private PreInsertLoadUpdateEventListener preInsertLoadUpdateEventListener;

	@PostConstruct
	public void registerListeners() {
		SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
		EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.PRE_INSERT).appendListener(preInsertLoadUpdateEventListener);
		registry.getEventListenerGroup(EventType.PRE_UPDATE).appendListener(preInsertLoadUpdateEventListener);
		registry.getEventListenerGroup(EventType.PRE_LOAD).appendListener(preInsertLoadUpdateEventListener);

		preInsertLoadUpdateEventListener.addPreInsertEventListeners(GenericEntity.class,
				BaseEntityPreInsertUpdateEventListener.class);
		preInsertLoadUpdateEventListener.addPreUpdateEventListeners(GenericEntity.class,
				BaseEntityPreInsertUpdateEventListener.class);

		preInsertLoadUpdateEventListener.addPreInsertEventListeners(User.class,
				UserPreInsertLoadUpdateEventListener.class);
		preInsertLoadUpdateEventListener.addPreUpdateEventListeners(User.class,
				UserPreInsertLoadUpdateEventListener.class);

	}
}
