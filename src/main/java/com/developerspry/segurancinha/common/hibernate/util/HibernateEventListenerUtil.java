package com.developerspry.segurancinha.common.hibernate.util;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Consumer;

import org.hibernate.event.spi.PreInsertEvent;

public class HibernateEventListenerUtil {

	public static <T> void preInsertSetProperte(PreInsertEvent event, String property, Object value,
			Consumer<T> setProperty) {
		String[] propriedades = event.getPersister().getEntityMetamodel().getPropertyNames();
		int propertyIndex = getPropertyIndex(propriedades, property);
		@SuppressWarnings("unchecked")
		T entiry = (T) event.getEntity();

		event.getState()[propertyIndex] = value;

		if (Objects.nonNull(setProperty)) {
			setProperty.accept(entiry);
		}
	}

	private static int getPropertyIndex(String[] properties, String property) {
		int propertyIndex = Arrays.asList(properties).indexOf(property);
		return propertyIndex;
	}

}
