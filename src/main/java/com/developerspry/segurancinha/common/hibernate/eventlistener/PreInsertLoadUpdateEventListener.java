package com.developerspry.segurancinha.common.hibernate.eventlistener;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import org.hibernate.event.spi.AbstractEvent;
import org.hibernate.event.spi.AbstractPreDatabaseOperationEvent;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreInsertEventListener;
import org.hibernate.event.spi.PreLoadEvent;
import org.hibernate.event.spi.PreLoadEventListener;
import org.hibernate.event.spi.PreUpdateEvent;
import org.hibernate.event.spi.PreUpdateEventListener;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * @author rubens.ferreira Gerencia os listenes de autenticação e cadastro
 */
@Component
public class PreInsertLoadUpdateEventListener
		implements PreInsertEventListener, PreUpdateEventListener, PreLoadEventListener {

	private static final long serialVersionUID = -6025324978977827826L;

	@Autowired
	private ApplicationContext applicationContext;

	private Map<Class<?>, Class<? extends PreInsertEventListener>> preInsertEventListeners;
	private Map<Class<?>, Class<? extends PreUpdateEventListener>> preUpdateEventListener;
	private Map<Class<?>, Class<? extends PreLoadEventListener>> preLoadEventListener;

	public PreInsertLoadUpdateEventListener() {
		preInsertEventListeners = new HashMap<Class<?>, Class<? extends PreInsertEventListener>>();
		preUpdateEventListener = new HashMap<Class<?>, Class<? extends PreUpdateEventListener>>();
		preLoadEventListener = new HashMap<Class<?>, Class<? extends PreLoadEventListener>>();
	}

	@Override
	public boolean onPreInsert(PreInsertEvent event) {

		executeListeners(getEventEntityClass(event), preInsertEventListeners,
				eventListener -> eventListener.onPreInsert(event));
		return false;
	}

	@Override
	public boolean onPreUpdate(PreUpdateEvent event) {

		executeListeners(getEventEntityClass(event), preUpdateEventListener,
				eventListener -> eventListener.onPreUpdate(event));
		return false;
	}

	@Override
	public void onPreLoad(PreLoadEvent event) {
		executeListeners(getEventEntityClass(event), preLoadEventListener,
				eventListener -> eventListener.onPreLoad(event));
	}

	public void addPreInsertEventListeners(Class<?> clazz,
			Class<? extends PreInsertEventListener> preInsertEventListenerClass) {
		preInsertEventListeners.put(clazz, preInsertEventListenerClass);
	}

	public void addPreUpdateEventListeners(Class<?> clazz,
			Class<? extends PreUpdateEventListener> preUpdateEventListenerClass) {
		preUpdateEventListener.put(clazz, preUpdateEventListenerClass);
	}

	public void addPreLoadEventListeners(Class<?> clazz,
			Class<? extends PreLoadEventListener> preLoadEventListenerClass) {
		preLoadEventListener.put(clazz, preLoadEventListenerClass);
	}

	private Class<?> getEventEntityClass(AbstractEvent event) {
		Class<?> clazz = null;
		if (event instanceof AbstractPreDatabaseOperationEvent) {
			clazz = ((AbstractPreDatabaseOperationEvent) event).getEntity().getClass();
		} else {
			clazz = ((PreLoadEvent) event).getEntity().getClass();
		}
		return clazz;
	}

	private <Listener> void executeListeners(Class<?> clazz, Map<Class<?>, Class<? extends Listener>> map,
			Consumer<Listener> action) {
		getEventListeners(clazz, map).forEach(action);

	}

	private <Listener> Collection<Listener> getEventListeners(Class<?> clazz,
			Map<Class<?>, Class<? extends Listener>> map) {
		Set<Listener> listeners = new HashSet<Listener>();

		Listener listener = null;

		// Recupera os listeners para as interfaces da classes
		for (Class<?> interfaceClass : clazz.getInterfaces()) {
			listener = getEventListener(interfaceClass, map);
			if (Objects.nonNull(listener)) {
				listeners.add(listener);
			}
		}

		// Recupera os listeners para a classe e suas subclasses
		do {
			listener = getEventListener(clazz, map);
			if (Objects.nonNull(listener)) {
				listeners.add(listener);
			}
		} while (!(clazz = clazz.getSuperclass()).equals(Object.class));

		return listeners;
	}

	private <Listener> Listener getEventListener(Class<?> clazz, Map<Class<?>, Class<? extends Listener>> map) {
		Listener eventListener = null;
		try {
			Class<? extends Listener> eventListenerClass = map.get(clazz);
			if (Objects.nonNull(eventListenerClass)) {

				try {
					eventListener = applicationContext.getBean(eventListenerClass);
				} catch (NoSuchBeanDefinitionException e) {
					eventListener = eventListenerClass.newInstance();
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
		}
		return eventListener;
	}

}
