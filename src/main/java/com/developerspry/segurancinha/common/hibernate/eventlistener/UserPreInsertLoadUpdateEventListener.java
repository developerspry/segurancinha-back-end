package com.developerspry.segurancinha.common.hibernate.eventlistener;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;

import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreUpdateEvent;

import com.developerspry.segurancinha.common.security.exception.InvalidKeyException;
import com.developerspry.segurancinha.common.security.manager.CryptoManager;
import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens.ferreira
 */
public class UserPreInsertLoadUpdateEventListener extends AbstractPreInsertLoadUpdateListener {

	private static final long serialVersionUID = -4870579007119480794L;

	/**
	 * Responsável por encriptar a senha e email antes de inserir
	 */
	@Override
	public boolean onPreInsert(PreInsertEvent event) {
		User user = (User) event.getEntity();
		List<String> properties = this.listProperties(event);

		String password = user.getPassword();
		String email = user.getEmail();
		String phone = user.getPhone();

		if (Objects.nonNull(email)) {
			try {
				email = CryptoManager.encrypt(email);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			
			if (Objects.nonNull(email)) {
				event.getState()[properties.indexOf(USER_EMAIL)] = email;
			}
		}
		
		if (Objects.nonNull(phone)) {
			try {
				phone = CryptoManager.encrypt(phone);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			
			if (Objects.nonNull(phone)) {
				event.getState()[properties.indexOf(USER_PHONE)] = phone;
			}
		}

		if (Objects.nonNull(password)) {
			try {
				password = CryptoManager.encryptSha1(password);
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(password)) {
				event.getState()[properties.indexOf(USER_PASSWORD)] = password;
			}
		}

		return false;
	}

	/**
	 * Responsável por encriptar a senha antes de atualizar
	 */
	@Override
	public boolean onPreUpdate(PreUpdateEvent event) {
		User user = (User) event.getEntity();
		List<String> properties = this.listProperties(event);

		String oldPassword = (String) event.getOldState()[properties.indexOf(USER_PASSWORD)];
		String password = user.getPassword();

		String oldEmail = (String) event.getOldState()[properties.indexOf(USER_EMAIL)];
		String email = user.getEmail();
		
		String oldPhone = (String) event.getOldState()[properties.indexOf(USER_PHONE)];
		String phone = user.getPhone();

		if (Objects.isNull(password) || "".equals(password.trim())) {
			event.getState()[properties.indexOf(USER_PASSWORD)] = oldPassword;
			user.setPassword(oldPassword);
		} else if (!oldPassword.equals(password)) {
			try {
				password = CryptoManager.encryptSha1(password);
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(password)) {
				event.getState()[properties.indexOf(USER_PASSWORD)] = password;
			}
			user.setPassword(password);
		} else {
			if (Objects.nonNull(password)) {
				event.getState()[properties.indexOf(USER_PASSWORD)] = password;
			}
		}
		if (Objects.isNull(email) || "".equals(email.trim())) {
			event.getState()[properties.indexOf(USER_EMAIL)] = oldEmail;
			user.setEmail(oldEmail);
		} else if (!oldEmail.equals(email)) {
			try {
				email = CryptoManager.encrypt(email);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(email)) {
				event.getState()[properties.indexOf(USER_EMAIL)] = email;
			}
			user.setEmail(email);
		} else {
			if (Objects.nonNull(email)) {
				event.getState()[properties.indexOf(USER_EMAIL)] = email;
			}
		}
		
		if (Objects.isNull(phone) || "".equals(phone.trim())) {
			event.getState()[properties.indexOf(USER_PHONE)] = oldPhone;
			user.setPhone(oldPhone);
		} else if (!oldPhone.equals(phone)) {
			try {
				phone = CryptoManager.encrypt(phone);
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			if (Objects.nonNull(phone)) {
				event.getState()[properties.indexOf(USER_PHONE)] = phone;
			}
			user.setPhone(phone);
		} else {
			if (Objects.nonNull(phone)) {
				event.getState()[properties.indexOf(USER_PHONE)] = phone;
			}
		}
		return false;
	}

}
