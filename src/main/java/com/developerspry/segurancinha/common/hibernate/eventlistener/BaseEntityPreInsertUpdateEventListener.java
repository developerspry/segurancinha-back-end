package com.developerspry.segurancinha.common.hibernate.eventlistener;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreUpdateEvent;

import com.developerspry.segurancinha.control.model.GenericEntity;
import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens.ferreira
 *
 */
public class BaseEntityPreInsertUpdateEventListener extends AbstractPreInsertLoadUpdateListener {

	private static final long serialVersionUID = -5801943553047375110L;

	@Override
	public boolean onPreInsert(PreInsertEvent event) {
		LocalDateTime currentDate = LocalDateTime.now();

		GenericEntity baseEntity = (GenericEntity) event.getEntity();
		User user = getAuthenticatedUser();
		List<String> properties = this.listProperties(event);

		if (Objects.nonNull(user)) {
			event.getState()[properties.indexOf(BASE_ENTITY_USER_INSERT)] = user;
			event.getState()[properties.indexOf(BASE_ENTITY_USER_UPDATE)] = user;
			baseEntity.setUserUpdate(user);
			baseEntity.setUserInsert(user);
		}

		event.getState()[properties.indexOf(BASE_ENTITY_DATE_INSERT)] = currentDate;
		event.getState()[properties.indexOf(BASE_ENTITY_DATE_UPDATE)] = currentDate;

		baseEntity.setDateInsert(currentDate);
		baseEntity.setDateUpdate(currentDate);

		return false;
	}

	@Override
	public boolean onPreUpdate(PreUpdateEvent event) {

		LocalDateTime currentDate = LocalDateTime.now();

		GenericEntity baseEntity = (GenericEntity) event.getEntity();
		User user = getAuthenticatedUser();
		List<String> properties = this.listProperties(event);

		if (Objects.nonNull(user)) {
			event.getState()[properties.indexOf(BASE_ENTITY_USER_UPDATE)] = user;
			baseEntity.setUserUpdate(user);
		}

		event.getState()[properties.indexOf(BASE_ENTITY_DATE_UPDATE)] = currentDate;
		baseEntity.setDateUpdate(currentDate);

		return false;
	}

}
