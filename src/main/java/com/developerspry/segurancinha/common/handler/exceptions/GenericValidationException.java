package com.developerspry.segurancinha.common.handler.exceptions;

import org.springframework.validation.Errors;

/**
 * @author rubens_ferreira
 *
 */
@SuppressWarnings("serial")
public class GenericValidationException extends RuntimeException {

	private Errors result;

	public GenericValidationException(Errors result, String message) {
		super(message);
		this.result = result;
	}

	public Errors getErrors() {
		return result;
	}

}