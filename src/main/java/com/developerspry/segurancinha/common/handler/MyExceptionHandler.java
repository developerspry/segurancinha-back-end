package com.developerspry.segurancinha.common.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.developerspry.segurancinha.common.handler.exceptions.AccessException;
import com.developerspry.segurancinha.common.handler.exceptions.BaseException;
import com.developerspry.segurancinha.common.handler.exceptions.GenericValidationException;
import com.developerspry.segurancinha.common.handler.exceptions.validation.ErrorValidationException;

/**
 * @author rubens_ferreira
 *
 */
@ControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(GenericValidationException.class)
	protected ResponseEntity<Object> handleInvalidRequest(RuntimeException e, WebRequest request) {
		ErrorValidationException error = new ErrorValidationException("InvalidRequest", e.getMessage(),
				(GenericValidationException) e);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.UNPROCESSABLE_ENTITY, request);
	}

	@ExceptionHandler(AccessDeniedException.class)
	protected ResponseEntity<Object> accessDeniedException(RuntimeException e, WebRequest request) {
		BaseException error = new AccessException("Forbidden", "Access Denied", HttpStatus.FORBIDDEN.value());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return handleExceptionInternal(e, error, headers, HttpStatus.FORBIDDEN, request);
	}

	@ExceptionHandler(Exception.class)
	@ResponseBody
	public BaseException exception(Exception e) {
		e.printStackTrace();
		return new BaseException(e, e.toString());
	}
}