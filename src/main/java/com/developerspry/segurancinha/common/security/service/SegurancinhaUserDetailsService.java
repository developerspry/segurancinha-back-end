package com.developerspry.segurancinha.common.security.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.developerspry.segurancinha.common.security.model.LoggedUser;
import com.developerspry.segurancinha.common.security.model.TokenInfo;
import com.developerspry.segurancinha.common.security.model.TokenInfoSystem;
import com.developerspry.segurancinha.common.security.model.TokenInfoUser;
import com.developerspry.segurancinha.control.model.User;
import com.developerspry.segurancinha.control.service.interfaces.IUserService;

/**
 * @author rubens_ferreira
 *
 */
@Service
public class SegurancinhaUserDetailsService implements UserDetailsService {
	
	@Autowired
	private IUserService userService;

	@Override
	public UserDetails loadUserByUsername(String dataLogin) throws UsernameNotFoundException {

		if (Objects.nonNull(dataLogin)) {
			TokenInfo tokenInfo = TokenInfo.getTokenInfoByPlainToken(dataLogin);

			switch (tokenInfo.getType()) {
			case USER:
				TokenInfoUser tokenInfoUsuario = (TokenInfoUser) tokenInfo;
				User user = this.userService.findByLogin(tokenInfoUsuario.getLogin());
				if (Objects.isNull(user)) {
					throw new UsernameNotFoundException("User " + tokenInfoUsuario.getLogin() + " Not Found.");
				}
				return new LoggedUser(user);

			case SYSTEM:
				TokenInfoSystem tokenInfoSystem = (TokenInfoSystem) tokenInfo;
				return new LoggedUser(tokenInfoSystem.getMac(), tokenInfoSystem);
			default:
				throw new UsernameNotFoundException("Undentified token type");
			}
		} else {
			throw new UsernameNotFoundException("Data not reported");
		}

	}

}
