package com.developerspry.segurancinha.common.security.model.dto;

import java.util.List;

import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens_ferreira
 *
 */
public class LoggedUserDTO {

	private long id;

	private String login;

	private String name;

	private String profileType;

	private List<String> roles;

	public LoggedUserDTO(User user) {
		this.id = user.getId();
		this.login = user.getLogin();
		this.name = user.getName();
		this.profileType = user.getProfileType();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getRoles() {
		return roles;
	}

	public String getProfileType() {
		return profileType;
	}

	public void setProfileType(String profileType) {
		this.profileType = profileType;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
