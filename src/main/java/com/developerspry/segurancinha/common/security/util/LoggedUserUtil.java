/**
 * 
 */
package com.developerspry.segurancinha.common.security.util;

import org.springframework.security.core.context.SecurityContextHolder;

import com.developerspry.segurancinha.common.security.model.LoggedUser;
import com.developerspry.segurancinha.control.model.User;

/**
 * @author rubens.ferreira
 *
 */
public class LoggedUserUtil {
	public static User getUser() {
		LoggedUser loggedUser = (LoggedUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return loggedUser.getUser() == null ? null : loggedUser.getUser();
	}
}
