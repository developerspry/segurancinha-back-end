package com.developerspry.segurancinha.common.security.manager;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Objects;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Component;

import com.developerspry.segurancinha.common.security.exception.InvalidKeyException;

/**
 * @author rubens_ferreira
 *
 */
@Component
public final class CryptoManager {

	private static final String SALT = "-#segurancinha#-";
	private static final String ALGORITHM = "AES";
	private static final String HASH_TYPE = "SHA-1";
	private static final String ENCODE_TYPE = "UTF-8";
	private static final String ENV_KEY = "SEGURANCINHA_KEY";

	public static void main(String[] args) {
		try {
			System.out.println(encrypt("81996763326"));
			System.out.println(encrypt("81995903435"));
			System.out.println(encrypt("81999999999"));
			
			System.out.println(encrypt("yuri.fernando@gmail.com"));
			System.out.println(encrypt("rubens.ferreira@gmail.com"));
			System.out.println(encrypt("paulo.renato@gmail.com"));
		} catch (InvalidKeyException e1) {
			e1.printStackTrace();
		}

		try {
			System.out.println(encryptSha1("cbpe@1.."));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		try {
			System.out.println(encryptSha1("cbpe@1.."));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public static String encrypt(String value) { String result = null; result =
	 * encrypt(value, null);
	 * 
	 * return result; }
	 */

	public static String encrypt(String value) throws InvalidKeyException {
		String result = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, getKeySpec(System.getenv(ENV_KEY)));
			result = new String(Base64.getEncoder().encode(cipher.doFinal(value.getBytes(ENCODE_TYPE))));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	public static String decrypt(String value) throws InvalidKeyException {

		String result = null;
		try {
			Cipher cipher = Cipher.getInstance(ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, getKeySpec(System.getenv(ENV_KEY)));
			result = new String(cipher.doFinal(Base64.getDecoder().decode(value.getBytes(ENCODE_TYPE))));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/*
	 * public static String decrypt(String value) { String result = null; result =
	 * decrypt(value, null);
	 * 
	 * return result; }
	 */

	private static SecretKeySpec getKeySpec(String key) {
		if (Objects.nonNull(key)) {
			return new SecretKeySpec((key + SALT).substring(0, 16).getBytes(), ALGORITHM);
		}

		return new SecretKeySpec(SALT.getBytes(), ALGORITHM);
	}

	public static String encryptSha1(String value) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		if (Objects.nonNull(value)) {
			StringBuilder hexString = new StringBuilder();
			for (byte b : MessageDigest.getInstance(HASH_TYPE).digest(value.getBytes(ENCODE_TYPE))) {
				hexString.append(String.format("%02x", 0xFF & b));
			}
			return hexString.toString();
		}
		return null;
	}

}
