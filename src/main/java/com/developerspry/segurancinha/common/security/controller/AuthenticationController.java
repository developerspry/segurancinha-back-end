package com.developerspry.segurancinha.common.security.controller;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.developerspry.segurancinha.common.security.model.LoggedUser;
import com.developerspry.segurancinha.common.security.model.dto.LoggedUserDTO;
import com.developerspry.segurancinha.control.service.interfaces.IUserService;

/**
 * @author rubens_ferreira
 *
 */
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

	@Autowired
	private IUserService userService;

	@PostMapping("/user")
	public ResponseEntity<LoggedUserDTO> authenticate(@RequestParam("login") String login,
			@RequestParam("password") String password,
			@RequestParam("ip") String ip,
			@RequestParam("action") String action, HttpServletRequest request,
			HttpServletResponse response) {

		LoggedUser loggedUser = (LoggedUser) request.getAttribute("LoggedUser");

		System.out.println("TOKEN -> (" + login + "): " + response.getHeader("x-auth-token"));

		if (Objects.nonNull(loggedUser)) {
			userService.findById(loggedUser.getUser().getId());
			return new ResponseEntity<LoggedUserDTO>(new LoggedUserDTO(loggedUser.getUser()), HttpStatus.OK);
		}
		return null;
	}

}