package com.developerspry.segurancinha.common.security.model;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author rubens_ferreira
 *
 */
public abstract class TokenInfo {
	public enum TypeToken {
		USER, SYSTEM
	}

	private String ip;

	private TypeToken type;

	private Date dateGenerate;

	public TokenInfo() {
	}

	public TokenInfo(String ip, TypeToken type, Date dataGeracao) {
		this.ip = ip;
		this.type = type;
		this.dateGenerate = dataGeracao;
	}

	protected String getField(String campo) {
		if (Objects.nonNull(campo)) {
			return campo + "_";
		} else {
			return "";
		}
	}

	public static TokenInfo getTokenInfoByPlainToken(String plainToken) {
		TokenInfo result = null;

		ObjectMapper mapper = new ObjectMapper();

		Class<?> clazz = TokenInfoUser.class;
		if (plainToken.toLowerCase().contains("\"type\":\"system\"")) {
			clazz = TokenInfoSystem.class;
		}

		try {
			result = (TokenInfo) mapper.readValue(plainToken, clazz);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public String toPlainToken() {
		ObjectMapper mapper = new ObjectMapper();
		String result = "";
		try {
			result = mapper.writeValueAsString(this);
		} catch (Exception e) {
			result = toString();
		}
		return result;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public TypeToken getType() {
		return type;
	}

	public void setType(TypeToken type) {
		this.type = type;
	}

	public Date getDateGenerate() {
		return dateGenerate;
	}

	public void setDateGenerate(Date dataGeracao) {
		this.dateGenerate = dataGeracao;
	}
}
