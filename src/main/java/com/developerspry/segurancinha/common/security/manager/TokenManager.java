package com.developerspry.segurancinha.common.security.manager;

import java.util.Objects;

import com.developerspry.segurancinha.common.security.exception.InvalidKeyException;
import com.developerspry.segurancinha.common.security.model.TokenInfo;

/**
 * @author rubens_ferreira
 *
 */
public class TokenManager {

	public static String generateToken(TokenInfo tokenInfo) {
		String result = null;
		try {
			result = CryptoManager.encrypt(tokenInfo.toPlainToken());
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static boolean validateToken(String token) throws InvalidKeyException {
		return Objects.nonNull(getTokenInfo(token));
	}

	public static TokenInfo getTokenInfo(String token) {
		try {
			String decriptedToken = CryptoManager.decrypt(token);
			if (Objects.nonNull(decriptedToken)) {
				return TokenInfo.getTokenInfoByPlainToken(decriptedToken);
			}
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		}

		return null;
	}
}
