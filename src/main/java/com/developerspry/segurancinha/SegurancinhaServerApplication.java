package com.developerspry.segurancinha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SegurancinhaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SegurancinhaServerApplication.class, args);
	}
}
